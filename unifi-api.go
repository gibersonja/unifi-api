package main

import (
	"fmt"
	"log"
	"os"
	"path"
	"regexp"
	"runtime"

	"github.com/unpoller/unifi"
)

func main() {
	if len(os.Args) < 2 {
		er(fmt.Errorf("no arguments given"))
	}

	var devname string
	var user string
	var pass string
	var addr string
	var dev string
	var err error

	args := os.Args[1:]
	for n := 0; n < len(args); n++ {
		arg := args[n]

		if arg == "--help" {
			fmt.Print("--help\t\tPrint this help message.\n")
			fmt.Print("--user=\t\tUsername for UniFi Controller.\n")
			fmt.Print("--pass=\t\tPassword for UniFi Controller.\n")
			fmt.Print("--name=\t\tDevice Name.\n")
			fmt.Print("abc\t\tDevice type query: (pdu, uap, udm, usg, usw, uxg)\n")
			fmt.Print("https://X.X.X.X\tIP or URL for UniFi Controller.\n")
			fmt.Print("\n\nids -> IDS (Device Name not required for IDS)\n")
			fmt.Print("pdu -> Smart Power PDU\n")
			fmt.Print("uap -> Access Poins\n")
			fmt.Print("udm -> Dream Machines\n")
			fmt.Print("usg -> 1Gb Gateways\n")
			fmt.Print("usw -> Switches\n")
			fmt.Print("uxg -> 10Gb Gateways\n\n\n")
			fmt.Print("EXAMPLE:\n\n")
			fmt.Printf("%s --user=<USERNAME> --pass=<PASSWORD> <URL or IP of Unifi CONTROLLER> --name=<NAME OF DEVICE> <3 Letter Device Type>\n", os.Args[0])
			fmt.Printf("%s --user=<USERNAME> --pass=<PASSWORD> <URL or IP of Unifi CONTROLLER> ids\n\n", os.Args[0])
			return
		}

		regex := regexp.MustCompile(`--user=(\S+)`)
		if regex.MatchString(arg) {
			user = regex.FindStringSubmatch(arg)[1]
		}

		regex = regexp.MustCompile(`--pass=(\S+)`)
		if regex.MatchString(arg) {
			pass = regex.FindStringSubmatch(arg)[1]
		}

		regex = regexp.MustCompile(`(https?://\d+\.\d+\.\d+\.\d+:?\d*)`)
		if regex.MatchString(arg) {
			addr = regex.FindStringSubmatch(arg)[1]
		}

		regex = regexp.MustCompile(`--name=(\S+)`)
		if regex.MatchString(arg) {
			devname = regex.FindStringSubmatch(arg)[1]
		}

		regex = regexp.MustCompile(`\w{3}`)
		if regex.MatchString(arg) {
			if arg == "pdu" || arg == "uap" || arg == "udm" || arg == "usg" || arg == "usw" || arg == "uxg" || arg == "ids" {
				dev = arg
			}
		}

	}

	c := &unifi.Config{
		User:     user,
		Pass:     pass,
		URL:      addr,
		SSLCert:  [][]byte{},
		ErrorLog: log.Printf,
		//DebugLog:  log.Printf,
		DebugLog:  nil,
		Timeout:   0,
		VerifySSL: false,
	}
	uni, err := unifi.NewUnifi(c)
	if err != nil {
		log.Fatalln("Error:", err)
	}

	sites, err := uni.GetSites()
	er(err)

	devices, err := uni.GetDevices(sites)
	er(err)

	var name string

	switch dev {
	case "ids":
		ids, err := uni.GetIDS(sites)
		er(err)
		for i := 0; i < len(ids); i++ {
			event := ids[i]
			dst_port := fmt.Sprintf("%d", event.DestPort)
			src_port := fmt.Sprintf("%d", event.SrcPort)
			flow_id := fmt.Sprintf("%d", event.FlowID)
			alert_severity := fmt.Sprintf("%d", event.InnerAlertSeverity)
			alert_gid := fmt.Sprintf("%d", event.InnerAlertGID)
			alert_signature_id := fmt.Sprintf("%d", event.InnerAlertSignatureID)
			time := fmt.Sprintf("%d", event.Time)
			timestamp := fmt.Sprintf("%d", event.Timestamp)
			date_time := event.Datetime.String()
			app_proto := event.AppProto
			cat_name := event.Catname
			dst_ip := event.DestIP
			dst_mac := event.DstMAC
			dst_ipasn := event.DstIPASN
			dst_ip_country := event.DstIPCountry
			event_type := event.EventType
			host := event.Host
			id := event.ID
			in_int := event.InIface
			alert_action := event.InnerAlertAction
			alert_category := event.InnerAlertCategory
			alert_signature := event.InnerAlertSignature
			key := event.Key
			msg := event.Msg
			proto := event.Proto
			site_id := event.SiteID
			site_name := event.SiteName
			src_name := event.SourceName
			src_ip := event.SrcIP
			src_ipasn := event.SrcIPASN
			src_ip_country := event.SrcIPCountry
			dst_ip_geo_asn := fmt.Sprintf("%d", event.DestIPGeo.Asn)
			dst_ip_geo_city := event.DestIPGeo.City
			dst_ip_geo_country_code := event.DestIPGeo.CountryCode
			dst_ip_geo_country_name := event.DestIPGeo.CountryName
			dst_ip_geo_continent_code := event.DestIPGeo.ContinentCode
			dst_ip_geo_lat := fmt.Sprintf("%f", event.DestIPGeo.Latitude)
			dst_ip_geo_lon := fmt.Sprintf("%f", event.DestIPGeo.Longitude)
			dst_ip_geo_org := event.DestIPGeo.Organization
			src_ip_geo_asn := fmt.Sprintf("%d", event.SourceIPGeo.Asn)
			src_ip_geo_city := event.SourceIPGeo.City
			src_ip_geo_country_code := event.SourceIPGeo.CountryCode
			src_ip_geo_country_name := event.SourceIPGeo.CountryName
			src_ip_geo_continent_code := event.SourceIPGeo.ContinentCode
			src_ip_geo_lat := fmt.Sprintf("%f", event.SourceIPGeo.Latitude)
			src_ip_geo_lon := fmt.Sprintf("%f", event.SourceIPGeo.Longitude)
			src_ip_geo_org := event.SourceIPGeo.Organization
			usg_ip_geo_asn := fmt.Sprintf("%d", event.USGIPGeo.Asn)
			usg_ip_geo_city := event.USGIPGeo.City
			usg_ip_geo_country_code := event.USGIPGeo.CountryCode
			usg_ip_geo_country_name := event.USGIPGeo.CountryName
			usg_ip_geo_continent_code := event.USGIPGeo.ContinentCode
			usg_ip_geo_lat := fmt.Sprintf("%f", event.USGIPGeo.Latitude)
			usg_ip_geo_lon := fmt.Sprintf("%f", event.USGIPGeo.Longitude)
			usg_ip_geo_org := event.USGIPGeo.Organization

			fmt.Printf("IDS_EVENT%d:id:%s,dst_port:%s,src_port:%s,flow_id:%s,alert_serverity:%s,alert_gid:%s,alert_signature_id:%s,time:%s,timestamp:%s,date_time:%s,app_proto:%s,cat_name:%s,dst_ip:%s,dst_mac:%s,dst_ipasn:%s,dst_ip_country:%s,event_type:%s,host:%s,in_int:%s,alert_action:%s,alert_category:%s,alert_signature:%s,key:%s,msg:%s,proto:%s,site_id:%s,site_name:%s,src_name:%s,src_ip:%s,src_ipasn:%s,src_ip_country:%s,dst_ip_geo_asn:%s,dst_ip_geo_city:%s,dst_ip_geo_country_code:%s,dst_ip_geo_country_name:%s,dst_ip_geo_centinent_code:%s,dst_ip_geo_lat:%s,dst_ip_geo_lon:%s,dst_ip_geo_org:%s,src_ip_geo_asn:%s,src_ip_geo_city:%s,src_ip_geo_country_code:%s,src_ip_geo_country_name:%s,src_ip_geo_centinent_code:%s,src_ip_geo_lat:%s,src_ip_geo_lon:%s,src_ip_geo_org:%s,usg_ip_geo_asn:%s,usg_ip_geo_city:%s,usg_ip_geo_country_code:%s,usg_ip_geo_country_name:%s,usg_ip_geo_centinent_code:%s,usg_ip_geo_lat:%s,usg_ip_geo_lon:%s,usg_ip_geo_org:%s", i, id, dst_port, src_port, flow_id, alert_severity, alert_gid, alert_signature_id, time, timestamp, date_time, app_proto, cat_name, dst_ip, dst_mac, dst_ipasn, dst_ip_country, event_type, host, in_int, alert_action, alert_category, alert_signature, key, msg, proto, site_id, site_name, src_name, src_ip, src_ipasn, src_ip_country, dst_ip_geo_asn, dst_ip_geo_city, dst_ip_geo_country_code, dst_ip_geo_country_name, dst_ip_geo_continent_code, dst_ip_geo_lat, dst_ip_geo_lon, dst_ip_geo_org, src_ip_geo_asn, src_ip_geo_city, src_ip_geo_country_code, src_ip_geo_country_name, src_ip_geo_continent_code, src_ip_geo_lat, src_ip_geo_lon, src_ip_geo_org, usg_ip_geo_asn, usg_ip_geo_city, usg_ip_geo_country_code, usg_ip_geo_country_name, usg_ip_geo_continent_code, usg_ip_geo_lat, usg_ip_geo_lon, usg_ip_geo_org)

		}
	case "pdu":
		device := devices.PDUs
		for i := 0; i < len(device); i++ {
			name = device[i].Name
			if devname == name {
				target := device[i]
				id := target.ID
				device_id := target.DeviceID
				gw_mac := target.GatewayMac
				internet := target.Internet.String()
				ip := target.IP
				license := target.LicenseState
				mac := target.Mac
				manufacturer_id := target.ManufacturerID.String()
				model := target.Model
				eol := target.ModelInEOL.String()
				outlet_ac_power_budget := target.OutletACPowerBudget.String()
				outlet_ac_power_consumption := target.OutletACPowerConsumption.String()
				outlet_enabled := target.OutletEnabled.String()
				outlet_table := target.OutletTable
				overheating := target.Overheating.String()
				ports := target.PortTable
				power_src := target.PowerSource.String()
				rx_bytes := target.RxBytes.String()
				tx_bytes := target.TxBytes.String()
				sn := target.Serial
				site_id := target.SiteID
				site_name := target.SiteName
				src_name := target.SourceName
				startup_timestamp := target.StartupTimestamp.String()
				state := target.State.String()
				total_max_power := target.TotalMaxPower.String()
				devtype := target.Type
				uptime := target.Uptime.String()
				version := target.Version
				version_display := target.DisplayableVersion
				load_avg1m := target.SysStats.Loadavg1.String()
				load_avg5m := target.SysStats.Loadavg5.String()
				load_avg15m := target.SysStats.Loadavg15.String()
				mem_buffer := target.SysStats.MemBuffer.String()
				mem_used := target.SysStats.MemUsed.String()
				mem_total := target.SysStats.MemTotal.String()

				for j := 0; j < len(ports); j++ {
					port := ports[j]
					ifname := port.Ifname
					dns := port.DNS //[]string
					int_fullduplex := port.FullDuplex.String()
					int_ip := port.IP
					int_mac := port.Mac
					int_media := port.Media
					int_name := port.Name
					int_net_name := port.NetworkName
					int_net_mask := port.Netmask
					int_num := port.NumPort.String()
					int_op_mode := port.OpMode
					int_poe_caps := port.PoeCaps.String()
					int_poe_class := port.PoeClass
					int_poe_current := port.PoeCurrent.String()
					int_poe_enabled := port.PoeEnable.String()
					int_poe_good := port.PoeGood.String()
					int_poe_mode := port.PoeMode
					int_poe_power := port.PoePower.String()
					int_poe_voltage := port.PoeVoltage.String()
					int_poe_port := port.PortPoe.String()
					int_index := port.PortIdx.String()
					int_conf_id := port.PortconfID
					int_rx_broadcast := port.RxBroadcast.String()
					int_rx_bytes := port.RxBytes.String()
					int_rx_dropped := port.RxDropped.String()
					int_rx_errors := port.RxErrors.String()
					int_rx_multicast := port.RxMulticast.String()
					int_rx_packets := port.RxPackets.String()
					int_rx_rate := port.RxRate.String()
					int_sfp_compliance := port.SFPCompliance
					int_sfp_current := port.SFPCurrent.String()
					int_sfp_found := port.SFPFound.String()
					int_sfp_part := port.SFPPart
					int_sfp_rev := port.SFPRev
					int_sfp_rx_fault := port.SFPRxfault.String()
					int_sfp_rx_power := port.SFPRxpower.String()
					int_sfp_sn := port.SFPSerial
					int_sfp_temp := port.SFPTemperature.String()
					int_sfp_tx_fault := port.SFPTxfault.String()
					int_sfp_tx_power := port.SFPTxpower.String()
					int_sfp_vendor := port.SFPVendor
					int_sfp_voltage := port.SFPVoltage.String()
					int_speed := port.Speed.String()
					int_tx_broadcast := port.TxBroadcast.String()
					int_tx_bytes := port.TxBytes.String()
					int_tx_dropped := port.TxDropped.String()
					int_tx_errors := port.TxErrors.String()
					int_tx_multicast := port.TxMulticast.String()
					int_tx_packets := port.TxPackets.String()
					int_tx_rate := port.TxRate.String()
					int_type := port.Type
					int_up := port.Up.String()

					fmt.Printf("INTERFACE%d:int_name:%s,ifname:%s,int_fullduplex:%s,int_ip:%s,int_mac:%s,int_media:%s,int_net_name:%s,int_net_mask:%s,int_num:%s,int_op_mode:%s,int_poe_caps:%s,int_poe_class:%s,int_poe_current:%s,int_poe_enabled:%s,int_poe_good:%s,int_poe_mode:%s,int_poe_power:%s,int_poe_voltage:%s,int_poe_port:%s,int_index:%s,int_conf_id:%s,int_rx_broadcast:%s,int_rx_bytes:%s,int_rx_dropped:%s,int_rx_errors:%s,int_rx_multicast:%s,int_rx_packets:%s,int_rx_rate:%s,int_sfp_compliance:%s,int_sfp_current:%s,int_sfp_found:%s,int_sfp_part:%s,int_sfp_rev:%s,int_sfp_rx_fault:%s,int_sfp_rx_power:%s,int_sfp_sn:%s,int_sfp_temp:%s,int_sfp_tx_fault:%s,int_sfp_tx_power:%s,int_sfp_vendor:%s,int_sfp_voltage:%s,int_speed:%s,int_tx_broadcast:%s,int_tx_bytes:%s,int_tx_dropped:%s,int_tx_errors:%s,int_tx_multicast:%s,int_tx_packets:%s,int_tx_rate:%s,int_type:%s,int_up:%s", j, int_name, ifname, int_fullduplex, int_ip, int_mac, int_media, int_net_name, int_net_mask, int_num, int_op_mode, int_poe_caps, int_poe_class, int_poe_current, int_poe_enabled, int_poe_good, int_poe_mode, int_poe_power, int_poe_voltage, int_poe_port, int_index, int_conf_id, int_rx_broadcast, int_rx_bytes, int_rx_dropped, int_rx_errors, int_rx_multicast, int_rx_packets, int_rx_rate, int_sfp_compliance, int_sfp_current, int_sfp_found, int_sfp_part, int_sfp_rev, int_sfp_rx_fault, int_sfp_rx_power, int_sfp_sn, int_sfp_temp, int_sfp_tx_fault, int_sfp_tx_power, int_sfp_vendor, int_sfp_voltage, int_speed, int_tx_broadcast, int_tx_bytes, int_tx_dropped, int_tx_errors, int_tx_multicast, int_tx_packets, int_tx_rate, int_type, int_up)
					for k := 0; k < len(dns); k++ {
						fmt.Printf(",dns%d:%s", k+1, dns[k])
					}
					fmt.Print("\n\n")
				}

				for k := 0; k < len(outlet_table); k++ {
					outlet := outlet_table[k]
					outlet_name := outlet.Name
					outlet_index := outlet.Index.String()
					outlet_relay_state := outlet.RelayState.String()
					outlet_current := outlet.OutletCurrent.String()
					outlet_power := outlet.OutletPower.String()
					outlet_power_factor := outlet.OutletPowerFactor.String()
					outlet_voltage := outlet.OutletVoltage.String()

					fmt.Printf("OUTLET%d:outlet_name:%s,outlet_index:%s,outlet_relay_state:%s,outlet_current:%s,outlet_power:%s,outlet_power_factor:%s,outlet_voltage:%s", k, outlet_name, outlet_index, outlet_relay_state, outlet_current, outlet_power, outlet_power_factor, outlet_voltage)
				}

				fmt.Printf("DEVICE%d:name:%s,id:%s,device_id:%s,gw_mac:%s,internet:%s,ip:%s,license:%s,mac:%s,manufacturer_id:%s,model:%s,eol:%s,outlet_ac_power_budget:%s,outlet_ac_power_consumption:%s,outlet_enabled:%s,overheating:%s,power_src:%s,rx_bytes:%s,tx_bytes:%s,sn:%s,site_id:%s,site_name:%s,src_name:%s,startup_timestamp:%s,state:%s,total_max_power:%s,type:%s,uptime:%s,version:%s,version_display:%s,load_avg1m:%s,load_avg5m:%s,load_avg15m:%s,mem_buffer:%s,mem_used:%s,mem_total:%s\n\n", i, name, id, device_id, gw_mac, internet, ip, license, mac, manufacturer_id, model, eol, outlet_ac_power_budget, outlet_ac_power_consumption, outlet_enabled, overheating, power_src, rx_bytes, tx_bytes, sn, site_id, site_name, src_name, startup_timestamp, state, total_max_power, devtype, uptime, version, version_display, load_avg1m, load_avg5m, load_avg15m, mem_buffer, mem_used, mem_total)
			}
		}
	case "uap":
		device := devices.UAPs
		for i := 0; i < len(device); i++ {
			name = device[i].Name
			if devname == name {
				target := device[i]
				atenna_table := target.AntennaTable //[]struct
				device_id := target.DeviceID
				gw_mac := target.GatewayMac
				id := target.ID
				ip := target.IP
				internet := target.Internet.String()
				kernel := target.KernelVersion
				last_uplink_mac := target.LastUplink.UplinkMac
				last_uplink_rport := fmt.Sprintf("%d", target.LastUplink.UplinkRemotePort)
				license := target.LicenseState
				//Skipping a bunch of LTE stuff...
				mac := target.Mac
				manufacturer_id := target.ManufacturerID.String()
				model := target.Model
				eol := target.ModelInEOL.String()
				ports := target.PortTable
				radio_table := target.RadioTable            //[]struct
				radio_table_stats := target.RadioTableStats //[]struct  need to do both of these for UDM also
				rx_bytes := target.RxBytes.String()
				tx_bytes := target.TxBytes.String()
				//ssh_sessions := target.SSHSessionTable //[]interface{}
				scanning := target.Scanning.String()
				sn := target.Serial
				site_id := target.SiteID
				site_name := target.SiteName
				src_name := target.SourceName
				startup_timestamp := target.StartupTimestamp.String()
				state := target.State.String()
				total_rx_bytes := target.TotalRxBytes.String()
				total_tx_bytes := target.TotalTxBytes.String()
				devtype := target.Type
				uptime := target.Uptime.String()
				version := target.Version
				load_avg1m := target.SysStats.Loadavg1.String()
				load_avg5m := target.SysStats.Loadavg5.String()
				load_avg15m := target.SysStats.Loadavg15.String()
				mem_buffer := target.SysStats.MemBuffer.String()
				mem_used := target.SysStats.MemUsed.String()
				mem_total := target.SysStats.MemTotal.String()

				for j := 0; j < len(ports); j++ {
					port := ports[j]
					ifname := port.Ifname
					dns := port.DNS //[]string
					int_fullduplex := port.FullDuplex.String()
					int_ip := port.IP
					int_mac := port.Mac
					int_media := port.Media
					int_name := port.Name
					int_net_name := port.NetworkName
					int_net_mask := port.Netmask
					int_num := port.NumPort.String()
					int_op_mode := port.OpMode
					int_poe_caps := port.PoeCaps.String()
					int_poe_class := port.PoeClass
					int_poe_current := port.PoeCurrent.String()
					int_poe_enabled := port.PoeEnable.String()
					int_poe_good := port.PoeGood.String()
					int_poe_mode := port.PoeMode
					int_poe_power := port.PoePower.String()
					int_poe_voltage := port.PoeVoltage.String()
					int_poe_port := port.PortPoe.String()
					int_index := port.PortIdx.String()
					int_conf_id := port.PortconfID
					int_rx_broadcast := port.RxBroadcast.String()
					int_rx_bytes := port.RxBytes.String()
					int_rx_dropped := port.RxDropped.String()
					int_rx_errors := port.RxErrors.String()
					int_rx_multicast := port.RxMulticast.String()
					int_rx_packets := port.RxPackets.String()
					int_rx_rate := port.RxRate.String()
					int_sfp_compliance := port.SFPCompliance
					int_sfp_current := port.SFPCurrent.String()
					int_sfp_found := port.SFPFound.String()
					int_sfp_part := port.SFPPart
					int_sfp_rev := port.SFPRev
					int_sfp_rx_fault := port.SFPRxfault.String()
					int_sfp_rx_power := port.SFPRxpower.String()
					int_sfp_sn := port.SFPSerial
					int_sfp_temp := port.SFPTemperature.String()
					int_sfp_tx_fault := port.SFPTxfault.String()
					int_sfp_tx_power := port.SFPTxpower.String()
					int_sfp_vendor := port.SFPVendor
					int_sfp_voltage := port.SFPVoltage.String()
					int_speed := port.Speed.String()
					int_tx_broadcast := port.TxBroadcast.String()
					int_tx_bytes := port.TxBytes.String()
					int_tx_dropped := port.TxDropped.String()
					int_tx_errors := port.TxErrors.String()
					int_tx_multicast := port.TxMulticast.String()
					int_tx_packets := port.TxPackets.String()
					int_tx_rate := port.TxRate.String()
					int_type := port.Type
					int_up := port.Up.String()

					fmt.Printf("INTERFACE%d:int_name:%s,ifname:%s,int_fullduplex:%s,int_ip:%s,int_mac:%s,int_media:%s,int_net_name:%s,int_net_mask:%s,int_num:%s,int_op_mode:%s,int_poe_caps:%s,int_poe_class:%s,int_poe_current:%s,int_poe_enabled:%s,int_poe_good:%s,int_poe_mode:%s,int_poe_power:%s,int_poe_voltage:%s,int_poe_port:%s,int_index:%s,int_conf_id:%s,int_rx_broadcast:%s,int_rx_bytes:%s,int_rx_dropped:%s,int_rx_errors:%s,int_rx_multicast:%s,int_rx_packets:%s,int_rx_rate:%s,int_sfp_compliance:%s,int_sfp_current:%s,int_sfp_found:%s,int_sfp_part:%s,int_sfp_rev:%s,int_sfp_rx_fault:%s,int_sfp_rx_power:%s,int_sfp_sn:%s,int_sfp_temp:%s,int_sfp_tx_fault:%s,int_sfp_tx_power:%s,int_sfp_vendor:%s,int_sfp_voltage:%s,int_speed:%s,int_tx_broadcast:%s,int_tx_bytes:%s,int_tx_dropped:%s,int_tx_errors:%s,int_tx_multicast:%s,int_tx_packets:%s,int_tx_rate:%s,int_type:%s,int_up:%s", j, int_name, ifname, int_fullduplex, int_ip, int_mac, int_media, int_net_name, int_net_mask, int_num, int_op_mode, int_poe_caps, int_poe_class, int_poe_current, int_poe_enabled, int_poe_good, int_poe_mode, int_poe_power, int_poe_voltage, int_poe_port, int_index, int_conf_id, int_rx_broadcast, int_rx_bytes, int_rx_dropped, int_rx_errors, int_rx_multicast, int_rx_packets, int_rx_rate, int_sfp_compliance, int_sfp_current, int_sfp_found, int_sfp_part, int_sfp_rev, int_sfp_rx_fault, int_sfp_rx_power, int_sfp_sn, int_sfp_temp, int_sfp_tx_fault, int_sfp_tx_power, int_sfp_vendor, int_sfp_voltage, int_speed, int_tx_broadcast, int_tx_bytes, int_tx_dropped, int_tx_errors, int_tx_multicast, int_tx_packets, int_tx_rate, int_type, int_up)
					for k := 0; k < len(dns); k++ {
						fmt.Printf(",dns%d:%s", k+1, dns[k])
					}
					fmt.Print("\n\n")
				}

				for k := 0; k < len(atenna_table); k++ {
					atenna := atenna_table[k]
					atenna_name := atenna.Name
					atenna_id := atenna.ID.String()
					atenna_wifi0_gain := atenna.Wifi0Gain.String()
					atenna_wifi1_gain := atenna.Wifi1Gain.String()

					fmt.Printf("ATENNA%d:atenna_name:%s,atenna_id:%s,atenna_wifi0_gain:%s,atenna_wifi1_gain:%s\n\n", k, atenna_name, atenna_id, atenna_wifi0_gain, atenna_wifi1_gain)
				}

				for l := 0; l < len(radio_table); l++ {
					radio := radio_table[l]
					radio_name := radio.Name
					radio_atenna_gain := radio.AntennaGain.String()
					radio_builtin_atenna_gain := radio.BuiltinAntGain.String()
					radio_current_atenna_gain := radio.CurrentAntennaGain.String()
					radio_channel := radio.Channel.String()
					radio_max_tx_power := radio.MaxTxpower.String()
					radio_min_tx_power := radio.MinTxpower.String()
					radio_min_rssi := radio.MinRssi.String()
					radio_min_rssi_enabled := radio.MinRssiEnabled.String()
					radio_nss := radio.Nss.String()
					radio_radio := radio.Radio
					radio_tx_power := radio.TxPower.String()
					radio_tx_power_mode := radio.TxPowerMode
					radio_wlan_group_id := radio.WlangroupID

					fmt.Printf("RADIO%d:radio_name:%s,radio_atenna_gain:%s,radio_builtin_atenna_gain:%s,radio_current_atenna_gain:%s,radio_channel:%s,radio_max_tx_power:%s,radio_min_tx_power:%s,radio_min_rssi:%s,radio_min_rssi_enabled:%s,radio_nss:%s,radio_radio:%s,radio_tx_power:%s,radio_tx_power_mode:%s,radio_wlan_group_id:%s\n\n", l, radio_name, radio_atenna_gain, radio_builtin_atenna_gain, radio_current_atenna_gain, radio_channel, radio_max_tx_power, radio_min_tx_power, radio_min_rssi, radio_min_rssi_enabled, radio_nss, radio_radio, radio_tx_power, radio_tx_power_mode, radio_wlan_group_id)
				}

				for m := 0; m < len(radio_table_stats); m++ {
					radio_stats := radio_table_stats[m]
					radio_stats_name := radio_stats.Name
					radio_stats_channel := radio_stats.Channel.String()
					radio_stats_radio := radio_stats.Radio
					radio_stats_gain := radio_stats.Gain.String()
					radio_stats_state := radio_stats.State
					radio_stats_tx_power := radio_stats.TxPower.String()
					radio_stats_tx_packets := radio_stats.TxPackets.String()

					fmt.Printf("RADIO%d_STATS:radio_stats_name:%s,radio_stats_channel:%s,radio_stats_radio:%s,radio_stats_gain:%s,radio_stats_state:%s,radio_stats_tx_power:%s,radio_stats_tx_packets:%s\n\n", m, radio_stats_name, radio_stats_channel, radio_stats_radio, radio_stats_gain, radio_stats_state, radio_stats_tx_power, radio_stats_tx_packets)
				}

				fmt.Printf("DEVICE%d:name:%s,device_id:%s,gw_mac:%s,id:%s,ip:%s,internet:%s,kernel:%s,last_uplink_mac:%s,last_uplink_rport:%s,license:%s,mac:%s,manufacturer_id:%s,model:%s,eol:%s,rx_bytes:%s,tx_bytes:%s,scanning:%s,sn:%s,site_id:%s,site_name:%s,src_name:%s,startup_timestamp:%s,state:%s,total_rx_bytes:%s,total_tx_bytes:%s,type:%s,uptime:%s,version:%s,load_avg1m:%s,load_avg5m:%s,load_avg15m:%s,mem_buffer:%s,mem_used:%s,mem_total:%s\n\n", i, name, device_id, gw_mac, id, ip, internet, kernel, last_uplink_mac, last_uplink_rport, license, mac, manufacturer_id, model, eol, rx_bytes, tx_bytes, scanning, sn, site_id, site_name, src_name, startup_timestamp, state, total_rx_bytes, total_tx_bytes, devtype, uptime, version, load_avg1m, load_avg5m, load_avg15m, mem_buffer, mem_used, mem_total)
			}
		}

	case "udm":
		device := devices.UDMs
		for i := 0; i < len(device); i++ {
			name = device[i].Name
			if devname == name {
				target := device[i]
				domain := target.DeviceDomain
				devid := target.DeviceID
				//geo_info := target.GeoInfo //map[string]GeoInfo
				id := target.ID
				ip := target.IP
				internet := target.Internet.String()
				is_access_point := target.IsAccessPoint.String()
				kernel := target.KernelVersion
				lan_ip := target.LanIP
				last_wan_ip := target.LastWlanIP
				license := target.LicenseState
				mac := target.Mac
				manufacturer_id := target.ManufacturerID.String()
				network_table := target.NetworkTable //[] struct
				overheating := target.Overheating.String()
				//rule_set_int := target.RulesetInterfaces //interface{}
				rx := target.RxBytes.String()
				tx := target.TxBytes.String()
				sn := target.Serial
				site_id := target.SiteID
				site_name := target.SiteName
				src_name := target.SourceName
				speed_test_latency := target.SpeedtestStatus.Latency.String()
				speed_test_upload := target.SpeedtestStatus.StatusUpload.String()
				speed_test_download := target.SpeedtestStatus.StatusDownload.String()
				speed_test_summary := target.SpeedtestStatus.StatusSummary.String()
				state := target.State.String()
				storage := target.Storage           //[]*Storage
				temperatures := target.Temperatures //[]Temperature
				devtype := target.Type
				uptime := target.Uptime.String()
				version := target.Version
				version_display := target.DisplayableVersion
				load_avg1m := target.SysStats.Loadavg1.String()
				load_avg5m := target.SysStats.Loadavg5.String()
				load_avg15m := target.SysStats.Loadavg15.String()
				mem_buffer := target.SysStats.MemBuffer.String()
				mem_used := target.SysStats.MemUsed.String()
				mem_total := target.SysStats.MemTotal.String()

				ports := target.PortTable
				for j := 0; j < len(ports); j++ {
					port := ports[j]
					ifname := port.Ifname
					dns := port.DNS //[]string
					int_fullduplex := port.FullDuplex.String()
					int_ip := port.IP
					int_mac := port.Mac
					int_media := port.Media
					int_name := port.Name
					int_net_name := port.NetworkName
					int_net_mask := port.Netmask
					int_num := port.NumPort.String()
					int_op_mode := port.OpMode
					int_poe_caps := port.PoeCaps.String()
					int_poe_class := port.PoeClass
					int_poe_current := port.PoeCurrent.String()
					int_poe_enabled := port.PoeEnable.String()
					int_poe_good := port.PoeGood.String()
					int_poe_mode := port.PoeMode
					int_poe_power := port.PoePower.String()
					int_poe_voltage := port.PoeVoltage.String()
					int_poe_port := port.PortPoe.String()
					int_index := port.PortIdx.String()
					int_conf_id := port.PortconfID
					int_rx_broadcast := port.RxBroadcast.String()
					int_rx_bytes := port.RxBytes.String()
					int_rx_dropped := port.RxDropped.String()
					int_rx_errors := port.RxErrors.String()
					int_rx_multicast := port.RxMulticast.String()
					int_rx_packets := port.RxPackets.String()
					int_rx_rate := port.RxRate.String()
					int_sfp_compliance := port.SFPCompliance
					int_sfp_current := port.SFPCurrent.String()
					int_sfp_found := port.SFPFound.String()
					int_sfp_part := port.SFPPart
					int_sfp_rev := port.SFPRev
					int_sfp_rx_fault := port.SFPRxfault.String()
					int_sfp_rx_power := port.SFPRxpower.String()
					int_sfp_sn := port.SFPSerial
					int_sfp_temp := port.SFPTemperature.String()
					int_sfp_tx_fault := port.SFPTxfault.String()
					int_sfp_tx_power := port.SFPTxpower.String()
					int_sfp_vendor := port.SFPVendor
					int_sfp_voltage := port.SFPVoltage.String()
					int_speed := port.Speed.String()
					int_tx_broadcast := port.TxBroadcast.String()
					int_tx_bytes := port.TxBytes.String()
					int_tx_dropped := port.TxDropped.String()
					int_tx_errors := port.TxErrors.String()
					int_tx_multicast := port.TxMulticast.String()
					int_tx_packets := port.TxPackets.String()
					int_tx_rate := port.TxRate.String()
					int_type := port.Type
					int_up := port.Up.String()

					fmt.Printf("INTERFACE%d:int_name:%s,ifname:%s,int_fullduplex:%s,int_ip:%s,int_mac:%s,int_media:%s,int_net_name:%s,int_net_mask:%s,int_num:%s,int_op_mode:%s,int_poe_caps:%s,int_poe_class:%s,int_poe_current:%s,int_poe_enabled:%s,int_poe_good:%s,int_poe_mode:%s,int_poe_power:%s,int_poe_voltage:%s,int_poe_port:%s,int_index:%s,int_conf_id:%s,int_rx_broadcast:%s,int_rx_bytes:%s,int_rx_dropped:%s,int_rx_errors:%s,int_rx_multicast:%s,int_rx_packets:%s,int_rx_rate:%s,int_sfp_compliance:%s,int_sfp_current:%s,int_sfp_found:%s,int_sfp_part:%s,int_sfp_rev:%s,int_sfp_rx_fault:%s,int_sfp_rx_power:%s,int_sfp_sn:%s,int_sfp_temp:%s,int_sfp_tx_fault:%s,int_sfp_tx_power:%s,int_sfp_vendor:%s,int_sfp_voltage:%s,int_speed:%s,int_tx_broadcast:%s,int_tx_bytes:%s,int_tx_dropped:%s,int_tx_errors:%s,int_tx_multicast:%s,int_tx_packets:%s,int_tx_rate:%s,int_type:%s,int_up:%s", j, int_name, ifname, int_fullduplex, int_ip, int_mac, int_media, int_net_name, int_net_mask, int_num, int_op_mode, int_poe_caps, int_poe_class, int_poe_current, int_poe_enabled, int_poe_good, int_poe_mode, int_poe_power, int_poe_voltage, int_poe_port, int_index, int_conf_id, int_rx_broadcast, int_rx_bytes, int_rx_dropped, int_rx_errors, int_rx_multicast, int_rx_packets, int_rx_rate, int_sfp_compliance, int_sfp_current, int_sfp_found, int_sfp_part, int_sfp_rev, int_sfp_rx_fault, int_sfp_rx_power, int_sfp_sn, int_sfp_temp, int_sfp_tx_fault, int_sfp_tx_power, int_sfp_vendor, int_sfp_voltage, int_speed, int_tx_broadcast, int_tx_bytes, int_tx_dropped, int_tx_errors, int_tx_multicast, int_tx_packets, int_tx_rate, int_type, int_up)
					for k := 0; k < len(dns); k++ {
						fmt.Printf(",dns%d:%s", k+1, dns[k])
					}
					fmt.Print("\n\n")
				}
				fmt.Printf("DEVICE%d:name:%s,domain:%s,device_id:%s,id:%s,ip:%s,internet:%s,is_access_point:%s,kernel:%s,lan_ip:%s,last_wan_ip:%s,license:%s,mac:%s,manufacturer_id:%s,overheating:%s,rx:%s,tx:%s,sn:%s,site_id:%s,site_name:%s,src_name:%s,speed_test_latency:%s,speed_test_upload:%s,speed_test_download:%s,speed_test_summary:%s,state:%s,type:%s,uptime:%s,version:%s,version_display:%s,load_avg1m:%s,load_avg5m:%s,load_avg15m:%s,mem_buffer:%s,mem_used:%s,mem_total:%s\n\n", i, name, domain, devid, id, ip, internet, is_access_point, kernel, lan_ip, last_wan_ip, license, mac, manufacturer_id, overheating, rx, tx, sn, site_id, site_name, src_name, speed_test_latency, speed_test_upload, speed_test_download, speed_test_summary, state, devtype, uptime, version, version_display, load_avg1m, load_avg5m, load_avg15m, mem_buffer, mem_used, mem_total)

				for l := 0; l < len(network_table); l++ {
					entry := network_table[l]
					net_name := entry.Name
					net_id := entry.ID
					net_dhcp_lease_count := entry.ActiveDhcpLeaseCount.String()
					net_dhcp_relay_enabled := entry.DhcpRelayEnabled.String()
					net_dhcp_dns1 := entry.DhcpdDNS1
					net_dhcp_dns2 := entry.DhcpdDNS2
					net_dhcp_dns3 := entry.DhcpdDNS3
					net_dhcp_dns4 := entry.DhcpdDNS4
					net_dhcpd_dns_enabled := entry.DhcpdDNSEnabled.String()
					net_dhcpd_enabled := entry.DhcpdEnabled.String()
					net_dhcpd_gw_enabled := entry.DhcpdGatewayEnabled.String()
					net_dhcpd_lease_time := entry.DhcpdLeasetime.String()
					net_dhcpd_start := entry.DhcpdStart
					net_dhcpd_stop := entry.DhcpdStop
					net_dhcpdv6_enabled := entry.Dhcpdv6Enabled.String()
					net_domain_name := entry.DomainName
					net_enabled := entry.Enabled.String()
					net_gw_int_name := entry.GatewayInterfaceName
					net_ip := entry.IP
					net_ip_subnet := entry.IPSubnet
					net_is_guest := entry.IsGuest.String()
					net_is_nat := entry.IsNat.String()
					net_mac := entry.Mac
					net_network_group := entry.Networkgroup
					net_rx_bytes := entry.RxBytes.String()
					net_tx_bytes := entry.TxBytes.String()
					net_rx_packets := entry.RxPackets.String()
					net_tx_packets := entry.TxPackets.String()
					net_site_id := entry.SiteID
					net_up := entry.Up.String()
					net_vlan_enabled := entry.VlanEnabled.String()

					fmt.Printf("NETWORK%d:net_name:%s,net_id:%s,net_dhcp_lease_count:%s,net_dhcp_relay_enabled:%s,net_dhcp_dns1:%s,net_dhcp_dns2:%s,net_dhcp_dns3:%s,net_dhcp_dns4:%s,net_dhcpd_dns_enabled:%s,net_dhcpd_enabled:%s,net_dhcpd_gw_enabled:%s,net_dhcpd_lease_time:%s,net_dhcpd_start:%s,net_dhcpd_stop:%s,net_dhcpdv6_enabled:%s,net_domain_name:%s,net_enabled:%s,net_gw_int_name:%s,net_ip:%s,net_ip_subnet:%s,net_is_guest:%s,net_is_nat:%s,net_mac:%s,net_network_group:%s,net_site_id:%s,net_up:%s,net_vlan_enabled:%s,net_rx_bytes:%s,net_tx_bytes:%s,net_rx_packets:%s,net_tx_packet:%s\n\n", l, net_name, net_id, net_dhcp_lease_count, net_dhcp_relay_enabled, net_dhcp_dns1, net_dhcp_dns2, net_dhcp_dns3, net_dhcp_dns4, net_dhcpd_dns_enabled, net_dhcpd_enabled, net_dhcpd_gw_enabled, net_dhcpd_lease_time, net_dhcpd_start, net_dhcpd_stop, net_dhcpdv6_enabled, net_domain_name, net_enabled, net_gw_int_name, net_ip, net_ip_subnet, net_is_guest, net_is_nat, net_mac, net_network_group, net_site_id, net_up, net_vlan_enabled, net_rx_bytes, net_tx_bytes, net_rx_packets, net_tx_packets)
				}
				for m := 0; m < len(temperatures); m++ {
					temp := temperatures[m]
					temp_name := temp.Name
					temp_type := temp.Type
					temp_value := fmt.Sprintf("%f", temp.Value)

					fmt.Printf("TEMPERATURES%d:temp_name:%s,temp_type:%s,temp_value:%v\n\n", m, temp_name, temp_type, temp_value)
				}
				for n := 0; n < len(storage); n++ {
					disk := storage[n]
					storage_name := disk.Name
					storage_mount := disk.MountPoint
					storage_size := disk.Size.String()
					storage_type := disk.Type
					storage_used := disk.Used.String()

					fmt.Printf("STORAGE%d:storage_name:%s,storage_mount:%s,storage_size:%s,storage_type:%s,storage_used:%s\n\n", n, storage_name, storage_mount, storage_size, storage_type, storage_used)
				}
			}
		}

	case "usg":
		//device := devices.USGs
		return
	case "usw":
		device := devices.USWs
		for i := 0; i < len(device); i++ {
			name = device[i].Name
			if devname == name {
				target := device[i]
				ip := target.IP
				id := target.ID
				mac := target.Mac
				manufacturer_id := target.ManufacturerID.String()
				model := target.Model
				eol := target.ModelInEOL.String()
				overheating := target.Overheating.String()
				powersrc := target.PowerSource
				powersrc_voltage := target.PowerSourceVoltage
				total_max_power := target.TotalMaxPower.String()
				sn := target.Serial
				state := target.State.String()
				rx := target.RxBytes.String()
				tx := target.TxBytes.String()
				devtype := target.Type
				uptime := target.Uptime.String()
				version := target.Version
				version_display := target.DisplayableVersion
				license := target.LicenseState
				fan := target.FanLevel.String()
				dev_id := target.DeviceID
				load_avg1m := target.SysStats.Loadavg1.String()
				load_avg5m := target.SysStats.Loadavg5.String()
				load_avg15m := target.SysStats.Loadavg15.String()
				mem_buffer := target.SysStats.MemBuffer.String()
				mem_used := target.SysStats.MemUsed.String()
				mem_total := target.SysStats.MemTotal.String()
				general_temp := target.GeneralTemperature.String()

				ports := target.PortTable
				for j := 0; j < len(ports); j++ {
					port := ports[j]
					ifname := port.Ifname
					dns := port.DNS //[]string
					int_fullduplex := port.FullDuplex.String()
					int_ip := port.IP
					int_mac := port.Mac
					int_media := port.Media
					int_name := port.Name
					int_net_name := port.NetworkName
					int_net_mask := port.Netmask
					int_num := port.NumPort.String()
					int_op_mode := port.OpMode
					int_poe_caps := port.PoeCaps.String()
					int_poe_class := port.PoeClass
					int_poe_current := port.PoeCurrent.String()
					int_poe_enabled := port.PoeEnable.String()
					int_poe_good := port.PoeGood.String()
					int_poe_mode := port.PoeMode
					int_poe_power := port.PoePower.String()
					int_poe_voltage := port.PoeVoltage.String()
					int_poe_port := port.PortPoe.String()
					int_index := port.PortIdx.String()
					int_conf_id := port.PortconfID
					int_rx_broadcast := port.RxBroadcast.String()
					int_rx_bytes := port.RxBytes.String()
					int_rx_dropped := port.RxDropped.String()
					int_rx_errors := port.RxErrors.String()
					int_rx_multicast := port.RxMulticast.String()
					int_rx_packets := port.RxPackets.String()
					int_rx_rate := port.RxRate.String()
					int_sfp_compliance := port.SFPCompliance
					int_sfp_current := port.SFPCurrent.String()
					int_sfp_found := port.SFPFound.String()
					int_sfp_part := port.SFPPart
					int_sfp_rev := port.SFPRev
					int_sfp_rx_fault := port.SFPRxfault.String()
					int_sfp_rx_power := port.SFPRxpower.String()
					int_sfp_sn := port.SFPSerial
					int_sfp_temp := port.SFPTemperature.String()
					int_sfp_tx_fault := port.SFPTxfault.String()
					int_sfp_tx_power := port.SFPTxpower.String()
					int_sfp_vendor := port.SFPVendor
					int_sfp_voltage := port.SFPVoltage.String()
					int_speed := port.Speed.String()
					int_tx_broadcast := port.TxBroadcast.String()
					int_tx_bytes := port.TxBytes.String()
					int_tx_dropped := port.TxDropped.String()
					int_tx_errors := port.TxErrors.String()
					int_tx_multicast := port.TxMulticast.String()
					int_tx_packets := port.TxPackets.String()
					int_tx_rate := port.TxRate.String()
					int_type := port.Type
					int_up := port.Up.String()

					fmt.Printf("INTERFACE%d:int_name:%s,ifname:%s,int_fullduplex:%s,int_ip:%s,int_mac:%s,int_media:%s,int_net_name:%s,int_net_mask:%s,int_num:%s,int_op_mode:%s,int_poe_caps:%s,int_poe_class:%s,int_poe_current:%s,int_poe_enabled:%s,int_poe_good:%s,int_poe_mode:%s,int_poe_power:%s,int_poe_voltage:%s,int_poe_port:%s,int_index:%s,int_conf_id:%s,int_rx_broadcast:%s,int_rx_bytes:%s,int_rx_dropped:%s,int_rx_errors:%s,int_rx_multicast:%s,int_rx_packets:%s,int_rx_rate:%s,int_sfp_compliance:%s,int_sfp_current:%s,int_sfp_found:%s,int_sfp_part:%s,int_sfp_rev:%s,int_sfp_rx_fault:%s,int_sfp_rx_power:%s,int_sfp_sn:%s,int_sfp_temp:%s,int_sfp_tx_fault:%s,int_sfp_tx_power:%s,int_sfp_vendor:%s,int_sfp_voltage:%s,int_speed:%s,int_tx_broadcast:%s,int_tx_bytes:%s,int_tx_dropped:%s,int_tx_errors:%s,int_tx_multicast:%s,int_tx_packets:%s,int_tx_rate:%s,int_type:%s,int_up:%s", j, int_name, ifname, int_fullduplex, int_ip, int_mac, int_media, int_net_name, int_net_mask, int_num, int_op_mode, int_poe_caps, int_poe_class, int_poe_current, int_poe_enabled, int_poe_good, int_poe_mode, int_poe_power, int_poe_voltage, int_poe_port, int_index, int_conf_id, int_rx_broadcast, int_rx_bytes, int_rx_dropped, int_rx_errors, int_rx_multicast, int_rx_packets, int_rx_rate, int_sfp_compliance, int_sfp_current, int_sfp_found, int_sfp_part, int_sfp_rev, int_sfp_rx_fault, int_sfp_rx_power, int_sfp_sn, int_sfp_temp, int_sfp_tx_fault, int_sfp_tx_power, int_sfp_vendor, int_sfp_voltage, int_speed, int_tx_broadcast, int_tx_bytes, int_tx_dropped, int_tx_errors, int_tx_multicast, int_tx_packets, int_tx_rate, int_type, int_up)
					for k := 0; k < len(dns); k++ {
						fmt.Printf(",dns%d:%s", k+1, dns[k])
					}
					fmt.Print("\n\n")
				}
				fmt.Printf("DEVICE%d:name:%s,ip:%s,id:%s,mac:%s,manufacturer_id:%s,model:%s,eol:%s,overheating:%s,powersrc:%s,powersrc_voltage:%s,total_max_power:%s,sn:%s,state:%s,rx_bytes:%s,tx_bytes:%s,type:%s,uptime:%s,version:%s,version_display:%s,license:%s,fan:%s,device_id:%s,general_temp:%s,load_avg1m:%s,load_avg5m:%s,load_avg15m:%s,mem_buffer:%s,mem_used:%s,mem_total:%s\n\n", i, name, ip, id, mac, manufacturer_id, model, eol, overheating, powersrc, powersrc_voltage, total_max_power, sn, state, rx, tx, devtype, uptime, version, version_display, license, fan, dev_id, general_temp, load_avg1m, load_avg5m, load_avg15m, mem_buffer, mem_used, mem_total)
			}
		}

	case "uxg":
		//device := devices.UXGs
		return
	}
}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("Error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}
